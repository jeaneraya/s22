/*
    Create functions which can manipulate our arrays.
    You may change the names on the initial list before you use the array varaibles.
*/

let registeredUsers = [

    "James Jeffries",
    "Maggie Williams",
    "Macie West",
    "Michelle Queen",
    "Angelica Smith",
    "Fernando Dela Cruz",
    "Mike Dy"
];

let friendsList = [];

/*
    
   1. Create a function which will allow us to register into the registeredUsers list.
        - this function should be able to receive a string.
        - determine if the input username already exists in our registeredUsers array.
            -if it is, show an alert window with the following message:
                "Registration failed. Username already exists!"
            -if it is not, add the new username into the registeredUsers array and show an alert:
                "Thank you for registering!"
        - Invoke and register a new user in the browser console.
        - In the browser console, log the registeredUsers array.

*/
    
function registerUser(user){
    let isRegistered = registeredUsers.includes(user);
    if (isRegistered === true){
        alert("Registration failed. Username already exists!");
    }else{
        registeredUsers.push(user);
        alert("Thank you for registering!");    
        console.log("Register User " + '"'+ user + '"');
    }
};
registerUser("Jayven");
console.log(registeredUsers);
/*
    2. Create a function which will allow us to add a registered user into our friends list.
        - This function should be able to receive a string.
        - Determine if the input username exists in our registeredUsers array.
            - If it is, add the foundUser in our friendList array.
                    -Then show an alert with the following message:
                        - "You have added <registeredUser> as a friend!"
            - If it is not, show an alert window with the following message:
                - "User not found."
        - Invoke the function and add a registered user in your friendsList in the browser console.
        - In the browser console, log the friendsList array in the console.

*/

function addFriendsList(friend){
    let isRegistered = registeredUsers.includes(friend);
    if (isRegistered === true){
        friendsList.push(friend);
        alert("You have added " + friend + " as a friend");
    }else{
        alert("User not found");
    }
}

addFriendsList("Jayven");

addFriendsList("Angelica Smith");



/*
    3. Create a function which will allow us to show/display the items in the friendList one by one on our console.
        - If the friendsList is empty show an alert: 
            - "You currently have 0 friends. Add one first."
        - Invoke the function.

*/

function displayFriendA(){
        if (friendsList.length === 0){
            alert("You currently have 0 friends. Add one first.");
        }else{
            friendsList.forEach(function(friend){
                console.log(friend);
            });
        };
    };
    
console.log("");
console.log("List of Friends:"); 
displayFriendA();
    
// STRETCH GOALS

/*
    4. Create a function which will display the amount of registered users in your friendsList.
        - If the friendsList is empty show an alert:
            - "You currently have 0 friends. Add one first."
        - If the friendsList is not empty show an alert:
            - "You currently have <numberOfFriends> friends."
        - Invoke the function in the browser console.
*/
let countFriend;
function displayFriendB(){
        if (friendsList.length === 0){
            alert("You currently have 0 friends. Add one first.");
        }else{
            console.log(friendsList.length);
        };
    };

console.log("");
console.log("Total number of Friends:"); 
displayFriendB();
/*
    5. Create a function which will delete the last registeredUser you have added in the friendsList.
        - If the friendsList is empty show an alert:
            - "You currently have 0 friends. Add one first."
        - Invoke the function.
        - In the browser console, log the friendsList array.

*/


function deleteFriend(){
    if(friendsList.length === 0){
        alert("You currently have 0 friends. Add one first.")
    }else{
        let removeFriend = friendsList.pop();
        return removeFriend;
    }
    
}

console.log("");
console.log("Deleted Friend is: " + '"' + deleteFriend() + '"');
console.log("New friendsList after pop:");
console.log(friendsList);

/*======================================================================================*/



// Try this for fun:
/*
   

    Instead of only deleting the last registered user in the friendsList delete a specific user instead.
        -You may get the user's index.
        -Then delete the specific user with splice().

*/

/*function deleteSpecificFriend(index,count,friend){
    friendsList.splice(index,count,friend);
    console.log(friendsList);
};

console.log("");
deleteSpecificFriend(0,1,"Macie West");*/








