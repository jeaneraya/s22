console.log("hi");

// ARRAY METHODS
/*
	Mutator Methods
		- seeks to modify the contents of an array.
		- are functions that mutate an array after they are created. These methods manipulate original array performing various tasks such as adding or removing elements.
*/

let fruit = ["Apple","Orange","Kiwi","Watermelon"];

/*
	push()
		- adds an element in the end of an array and returns the array's length

		Syntax:
			arrayName.push(element)
*/

console.log("Current fruits array: ");
console.log(fruit);

//Adding element/s

fruit.push("Mango");
console.log(fruit);

let fruitLength = fruit.push("Melon");
console.log(fruitLength);

console.log(fruit);

fruit.push("Avocado","Guava","Durian");

console.log(fruit);

/*
	pop()
		- removes the last element in our array and returns the removed element (when applied inside a variable)

		Syntax:
			arrayName.pop();
*/

console.log("");
let removeFruit = fruit.pop();
console.log(removeFruit);
console.log("Mutated Array from the pop method:");
console.log(fruit);

/*
	unshift()
		- adds 1 or more elements at the beginning of an array
		- returns the length (when presented inside a variable)

		Syntax:
			arrayName.unshift(element);
*/
console.log("");
fruit.unshift("Pineapple","Banana");
console.log("Mutated array from the unshift methods");
console.log(fruit);

/*
	shift()
		- removes an element at the beginning or our array and return the removed element

		Syntax:
			arrayName.shift()
*/

let removeFruit2 = fruit.shift();
console.log(removeFruit2);
console.log("Mutated array from the shift method");
console.log(fruit);

/*
	splice()
		- allows to simultaneously remove elements from a specified index number and adds an element

		Syntax:
			arrayName.splice(startingIndex,deleteCount,elementToBeAdded);
*/

let spliceFruit = fruit.splice(1,2,"Cherry","Lychee");
console.log("Mutated array from splice methods");
console.log(fruit);
console.log(spliceFruit);

let removedUsingSplice = fruit.splice(3,2);
console.log(fruit);
console.log(removedUsingSplice);

/*
	sort()
		- rearranges the array element in alphanumeric order

		Syntax:
			arrayName.sort()
*/

/*fruit.sort();
console.log("Mutated array from sort method");
console.log(fruit);*/

let mixedArr = [12,"May",36,94,"August",5,3,"September"];
console.log(mixedArr.sort());

/*
	reverse()
		- it reverses the order of the element in an array

		Syntax:
			arrayName.reverse()
*/

fruit.reverse();
console.log("Mutated array from reverse method");
console.log(fruit);

// FOr sorting the items in descending order

fruit.sort().reverse();
console.log(fruit);

	/*MINI ACTIVITY:
	 - Debug the function which will allow us to list fruits in the fruits array.
	 	-- this function should be able to receive a string.
	 	-- determine if the input fruit name already exist in the fruits array.
	 		*** If it does, show an alert message: "Fruit already listed on our inventory".
	 		*** If not, add the new fruit into the fruits array ans show an alert message: "Fruit is not listed in our inventory."
	 	-- invoke and register a new fruit in the fruit array.
	 	-- log the updated fruits array in the console
	 	
	*/

function registerFruit (fruitName) {
	 		let doesFruitExist = fruit.includes(fruitName);

	 		if(doesFruitExist) {
	 			alert(fruitName + " is already on our inventory");
	 		} else {
	 			fruit.push(fruitName);
	 			alert("Fruit is now listed in our inventory");
	 		
	 		}
	 	}

//registerFruit("Watermelon");

/*
	MINI ACTIVTY - Array Mutators

		- Create an empty array
		- use push() method and unshift() method to add elements in your array
		- use the pop() and shift() method to remove elements in your array
		- use sort to organize your array 
*/
console.log("");
console.log("");
console.log("");
let myArray = [];

console.log(myArray);

myArray.push("Jean","Victoria","Kaysha","Jayven","Jeraldine","Glaiza","Prezel");
console.log("Array using push method:");
console.log(myArray);

console.log("");

myArray.unshift("Michelle","Jennyline");
console.log("Array using unshift method:");
console.log(myArray);

console.log("");

myArray.pop();
console.log("Array using pop method:");
console.log(myArray);

console.log("");

myArray.shift();
console.log("Array using shift method:");
console.log(myArray);

console.log("");

myArray.sort();
console.log("Array using sort method:");
console.log(myArray);

/*
	Non-Mutator Methods
		- these are functions or methods that do not modify of change an array after they are created
		- these methods do not manipulate the original array performing various tasks such as returning elements from an array and combining arrays and printing the output
*/
console.log("");
console.log("");
console.log("");
let countries = ["US","PH","CAN","SG","THA","FR","PH","UKR","DH"];
console.log(countries);

/*
	indexOf()
		- returns the index number of the first matching element found in an array. If no match found, the result will be -1. The search process will be done from our first element proceeding to the last element

		Syntax:
			arrayName.indexOf(searchValue);
			arrayName.indexOf(searchValue,fromIndex);
*/

let firstIndex = countries.indexOf("PH");
console.log("Result of indexOf method: " + firstIndex);

firstIndex = countries.indexOf("PH",4);
console.log("Result of indexOf method: " + firstIndex);

firstIndex = countries.indexOf("PH",7);
console.log("Result of indexOf method: " + firstIndex);

console.log("");

/*
	lastIndexOf()
		- return the index number of the last matching element found in an array. The search process will be done from the last proceeding to the first 

		Syntax:
			arrayName.lastIndexOf(searchValue)
			arrayName.lastIndexOf(searchValue, fromIndex)
*/

console.log("");
let lastIndex = countries.lastIndexOf("PH");
console.log("Result of lastIndexOf method: " + lastIndex);
lastIndex = countries.lastIndexOf("PH",4);
console.log("Result of lastIndexOf method: " + lastIndex);

/*
	slice()
		- portions/slices element from our array and returns a new array

		Syntax:
			arrayName.slice(startingIndex)
			arrayName.slice(startingIndex, endingIndex)
*/

console.log(countries);

let slicedArrA = countries.slice(4);
console.log(slicedArrA);

console.log(countries);

let slicedArrB = countries.slice(2,5);
console.log(slicedArrB);

console.log("");
let slicedArrC = countries.slice(-3);
console.log(slicedArrC);

/*
	toString();
		- this method returns an array as a string separated by comas
		- is used internally by JS when an array needs to be displayed as a text(like in HTML), or when an object/array needs to be used as a string.

		Syntax:
			arrayName.toString()
*/

console.log("");
let stringArray = countries.toString();
console.log(stringArray);

/*
	concat()
		- combines two or more arrays and returns the combined resuslt

		Syntax:
			arrayNameA.concat(arrayNameB)
			arrayNameA(elementA)
*/

let taskArrayA = ["drink HTML","eat Javascript"];
let taskArrayB = ["inhale CSS","breath SASS"];
let taskArrayC = ["get git","be node"];

console.log("");
let tasks = taskArrayA.concat(taskArrayB);
console.log(tasks);

let alltasks = taskArrayA.concat(taskArrayB,taskArrayC);
console.log(alltasks);

let combineTask = taskArrayA.concat("smell express","throw react");
console.log(combineTask);

/*
	join()
		- returns an array as a string
		- it does not change the original array
		- we can use any separator. The default is comma(,)

		Syntax:
			arrayName.join(separatorString)
*/

let students = ["Jean","Jayven","Kaysha","Glaiza"];
console.log(students.join("-"));
console.log(students.join(" "));


/*
	Iteration Methods
		- are loops designed to perform repititive tasls in an array, used to manipulating array data resulting in complex tasks.
		- normally work with a function supplied as an argument
		- it aims to evaluate each element in an array
*/

/*
	forEach()
		- similar to for loop that iterates on each array element.

		Syntax:
			arrayName.forEach(function(individualElement){
			statement;
			});
*/

console.log("This is forEach")
alltasks.forEach(function(task){
	console.log(task);
});

//Using forEach with conditional statements
console.log("");
let filteresTasks = [];

alltasks.forEach(function(task){
	console.log(task);
	if(task.length > 10){
		filteresTasks.push(task);
	};
});

console.log(alltasks);
console.log(filteresTasks);

/*
	map()
		- iterates on each element and returns a new array with different values depending on the result of the function operation

		Syntax:
			arrayName.map(function(individualElement){
				statement;
			});
*/

let numbers = [1,2,3,4,5];
let numberMap = numbers.map(function(number){
	console.log(number);
	return number*number;
})

console.log("Original Array: " + numbers);
console.log("Result of the Map Method:");
console.log(numberMap);

/*
	every()
		- checks if all elements in an array met the given condition. Returns a true value if all elements meet the condition and false if otherwise

		Syntax:
			arrayName.every(function(individualElement){
				return expression/condition;
			});
*/

let allValid = numbers.every(function(number){
	return (number < 3);
});

console.log("Result of every method: " + allValid);

/*
	some()
		- checks if at least one element in the array meet the given condition. Returns a true value is at least one of the elements meets the given condition and false if otherwise.

		Syntax:
			arrayName.some(function(individualElement){
				return expression/condition
			})
*/

let someValid = numbers.some(function(number){
	return (number < 3);
});

console.log("Result of some method: " + someValid);

/*
	filter()
		- returns a new array that contains elements which meets the given condition. Returns an empty array if no elements were found(that satisfied the given condition)

		Syntax:
			arrayName.filter(function(individualElement){
				return expression/condition
			})
*/

let filteredValid = numbers.filter(function(number){
		return (number < 5);
});

console.log("Result of filter method: " + filteredValid);

/*
	includes()
		- checks if the argument passed can be found in an array
		- this method can be chained or chain after another method. The result of the first method is used on the second method until all chained methods have been resolved.
		
		Syntax:
			arrayName.includes()
*/

let products = ["mouse","KEYBOARD","laptop","monitor"];

let filteredProducts = products.filter(function(product){
		return product.toLowerCase().includes("a");
});

console.log(filteredProducts);